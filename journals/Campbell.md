## DATE: Wednesday, 17JAN2024
Lost to user error with GIT.

## DATE: Wednesday, 18JAN2024
Today we focused on getting the project off the ground. Things appeared to go smoothly until Gage ran into some errors with attaching his database(db) in pgadmin. Eventually we narrowed down the issue our .yaml, where we were using the default values and not updating to our new database.We eventually copied and pasted the entirety of files from myself and Matt to Gage to try and fix, but still got the error "UNABLE TO CONNECT TO SERVER: connection failed: FATAL: password authentication failed for user 'myuser'. Eventually we figured out the issue was he had persistent data in his local memory causing conflicts.

## DATE: Thursday, 19JAN2024
Today Gage and Matt focused on Auth while I took on reworking our db tables. During the compose build, there were issues with our relationships not allowing null values in some of our tables. I discovered this while I was building out test cases to use with the db. While attempting to build PokeInstance in the database, errors kept populating about items not pulling (HelmID, ArmorID, PokeSpeciesID, etc.). In order to create PokeInstance, an instance of each item had to be created, even in cases where the vision of the project does not necessarily require an input (Helm, Weapon, Armor). As I went through, I ended up refactoring and adding null to many items in the tables.

## DATE: Friday, 20JAN2024
Today I ended up finishing the tables early, and began trying to code along and catch up to Gage and Matt on the Auth logic. They have made some very good progress while I fought the db. While the intent was for me to catch up on Auth, due to new issues arising with the tables, I ended up having to go back and refactor a few more items in it. Towards the end of the day, Sells returned and I spent an hour or so catching him up on the vision and progress of the project.

## DATE: Tuesday, 23JAN2024
Today we began working on our routes and endpoints. Auth is done and working! So we can now get to the real work. Almost immediately we ran into an issue where we didn't quite know how to persist the pokeball pseudo inventory across multiple pages. Because our tables actually weren't built yet, I was assigned to send our table creation into migration files, as well as finishing testing for the database. We ran into some more issues with our table structure, so I ended up having to do some research and doing more refactoring... I live in the database at this point.

## DATE: Wednesday, 24JAN2024
We had some more professional programmers look over our table structure, and they reccommended we separate our migrations into their own steps to help with debugging. So since tables are my jam at this point, I took that on, as well as addressing some issues with type errors while they were testing out the Auth/Account features. Gage's and Matt's brilliant code managed to pull the poke-data we wanted, and I managed to help him socket it into the db!!! Toward the end of the day it was requested that I make the insertions for the balls table so we could begin testing that endpoint on the next day.

## DATE: Thursday, 25JAN2024
Today was supposed to be building out the git issues so we could start a ticketting system, however, today was very unproductive to start. Our 4th team member, David Sells, expressed concerns with how we have organized the project. While I understand his concerns, and his want to do pair programming, due to how much time he has missed in the class the last couple of weeks and how much he is pushing it, it comes across and him wanting his hand held across the finish line. We explained that at this point, it is impossible at the current stage of the program, we are unable to break features out since everything was dependant on the data tables and the authentication. He was very argumentative and combattive, personnally attacking Gage whenever he could. We eventually called in a moderator, which he was afraid of calling one in, and eventually the argument ended to really no conclusion. This was an entire day we lost on the project it felt like. After the disagreement, Sells was in the room but not on camera and microphone, so we managed to make some progress. I figured out on the sql side how to insert a pokemon from the pokedex to the pokeinstance, and how to send up an update as well as finishing out breaking out the tables in the after hours.

# DATE: Friday, 26JAN2024
It occured to me during our testing that the way I have designed the migrations removes the tables instead of clearing the tables... well that was a learning experience. So today I focused on creating insertion and deletion migrations instead of the insertion drop table migrations I had previously developed. It took some time to code and test every single one, but eventually I got that done while Gage and Matt worked on the create trainer function.

# DATE: Monday, 29JAN2024
