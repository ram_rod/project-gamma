# MT Journal
<details>
<summary>DATE: Wednesday, 17JAN2024</summary>
- Edited the given docker-compose.yaml file with team (dubbed "mob-coding") where we all worked on it in parallel, first one that has a working change/fix "wins". We ended up using my iteration. Gage is unable to log in to pgadmin for some reason.
</details>


<details>
<summary>DATE: Thursday, 18JAN2024</summary>
- Bart chimed in on our plight with Gage's issue with pgadmin... It turns out, there were a few things that weren't quite right with our yaml. We worked together and got the yaml fixed up and everyone is on the same page.
- I wrote sql to build the data tables prior to the official start of the project and we were able to incorporate it in the fastapi migrations, however, there was a unique constraint error. It appears that the initial way that we had the relationships established (MANY fk in pokeinstance to pokedex) was not correct. It looks like we may have gotten it fixed by removing all but one of the fk relationships (id).
</details>


<details>
<summary>DATE: Friday, 19JAN2024</summary>
- Rode the struggle bus today. Authenticator is mostly done, I think? I have to get the queries set up to make the accounts, which, I don't know how to do, or I do (because of previous projects) and I'm missing a connection.
- We made a some great progress today in my mind.
</details>


<details>
<summary>DATE: Monday, 22JAN2024</summary>
- Excellent work today. We got AUTH in a working state. I managed to get a lot of it done one Saturday (did not push up any commits) and finished up my work fairly early on today. I asked for some feedback and got great stuff from Bart. I pushed my changes up to main (Pipeline failure for indentation formatting) but the code works. Next up is figuring out how to get the API endpoint / routes set up and written. __King's to Gage__ for rewriting / fixing the database tables (removing quotations that were everywhere). Without that effort, the code would not have been as "easy" (still wasn't easy to me) to write.
</details>


<details>
<summary>DATE: Tuesday, 23JAN2024</summary>
- We nailed down some details about back end routes and endpoints. Not a lot of coding today, but we have been exploring how we want to get the first data table (pokedex). David took our monolith migration (data table with all of the steps) and broke them out in to their own migration step. Dalonte strongly recommended that we do that, and we agreed that it was a better idea even though our initial implementation worked.
- We are still kind of toying around with how to get our pokedex table populated (need to get 151 pokemon and selected stats, then sent to psql? local storage?)
</details>


<details>
<summary>DATE: Wednesday, 24JAN2024</summary>
- Great progress today! We got our 3rd party API "mass call" operational!! It makes 151 requests and pulls out only the info we want and writes it to our pokedex table. It should only occur once when the app is started and then the data will be stored on our server DB to be used later. Our thought is to have the pokedex live on the server instead of the frontend so that users can't manipulate the data and give themselves god-tier 'mons. It has a check built in to it that will make sure that the calls are only performed once (at app startup). Such a win!
</details>


<details>
<summary>DATE: Thursday, 25JAN2024</summary>
- Not a lot of progress today. We reorganized our Git issues and got one end point finished.
- (Update) I ended up doing some after hours work with Gage and got one another endpoint to a pretty good spot that I will present to the group for review on Friday.
</details>


<details>
<summary>DATE: Friday, 26JAN2024</summary>
- David made great strides reformatting the database table migrations. We found some more issues that we need to flesh out a bit more. I got started on an issue for another endpoint that needed to be created. The endpoints needed the db to be refined and we thought it would be a good idea to add "seed data" to test endpoints out with.
</details>


<details>
<summary>DATE: Saturday, 27JAN2024</summary>
- I spent some time on friday night and Saturday morning finishing up the endpoint that I started. It passes in an id that is randomly generated and then inserts a row from the pokedex in to the poke_instances table.
</details>


<details>
<summary>DATE: Sunday, 28JAN2024</summary>
- A little more work completed. Two endpoints away from complete for MVP.
</details>


<details>
<summary>DATE: Monday, 29JAN2024</summary>
- More work completed.
</details>


<details>
<summary>DATE: Tuesday, 30JAN2024</summary>
- More work done.
</details>


<details>
<summary>DATE: Wednesday, 31JAN2024</summary>
- Backend is finished for MVP with the exception of changing password. David tried to get it, I tried to get it, no dice. We are moving forward to frontend.
</details>


<details>
<summary>DATE: Thursday, 01FEB2024</summary>
Didn't really get much done today. Mostly just trying to figure out what we wanted to use for routing, auth. Toyed around with Vue Router because vite pointed toward it, no dice. We spoke with Bart because we were experiencing decision paralysis. We settled on using React Router, SWR.
</details>


<details>
<summary>DATE: Friday, 02FEB2024</summary>
Login feature complete! Frontend AUTH seems to be working. Token is being grabbed from the back and used in the front!
Got Shadcn and Tailwindcss in to the frontend. This was a bit of a learning curve. Unlike previous projects where a cdn was provided from bootstrap (we could have done the same with Tailwindcss), we wanted components from shad, they are more appealing. Because of that, we have to install components as we go as needed, but it's pretty cool that the components are easy to modify. There are still Tailwind components available for us to use too. I got a basic navbar created so we can move around the app as needed. Still work to be done. We'll still need to lock down the app for logged in users, but that can come later.
</details>


<details>
<summary>DATE: Saturday, 03FEB2024</summary>
Adjusted the navbar a bit, spruced up the login page card. Gage figured out the sign up page. We tried getting SWR to work but we couldn't but axios did work. We wanted SWR for the data GET fetch and axios for POST, PUT, DELETE which we have a working POST (sign up!). The dat fetching wasn't working for the Profile page but then we made the switch over to axios wholly and got some of the  kinks worked out, just not all. We also found an issue with our Profile get query, if a mons party doesn't exist, the query fails.
</details>


<details>
<summary>DATE: Sunday, 04FEB2024</summary>
A lot accomplished over the weekend! We went from no front end to almost complete. Fixed up some css. Started working on navbar. Getting only the users to be able to see the relevant links, not logged in users can only see their relevant links.
</details>



<details>
<summary>DATE: Monday, 05FEB2024</summary>
Ran in to an issue with the nav bar not being able to log out reliably. I used SWR to try to mutate the token which in turn messed up a bunch of other stuff I think. I was using the SWRConfig which after further reading the docs was probably not the right choice. I switched over to what I thought was the local SWR stuff. No dice... the logout still doesn't work correctly.
</details>


<details>
<summary>DATE: Thursday 08FEB2024</summary>
Deployed our webapp. It worked... kinda... There were a lot of issues that came up that appear to be race conditions, but uncertain...
</details>

<details>
<summary>DATE: Thursday 09FEB2024</summary>
Finished UI changes! We rolled back main from deployment for presentation to instructor and continued development. BUT our MVP is finished!
</details>