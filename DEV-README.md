# Module3 Project Gamma

## Team:

-   **Gage Douglas** - Full-Stack Software Engineer
-   **Matt Thomas** - Full-Stack Software Engineer
-   **David Campbell** - Full-Stack Software Engineer

---

### Index

- [Module3 Project Gamma](#module3-project-gamma)
  - [Team:](#team)
    - [Index](#index)
    - [About the project](#about-the-project)
    - [Backend Overview](#backend-overview)
    - [Frontend Overview](#frontend-overview)
  - [Installation](#installation)
    - [Prerequisites](#prerequisites)
    - [Getting Started](#getting-started)
    - [Clone the Repository](#clone-the-repository)
    - [Optional: Initialize a Virtual Environment](#optional-initialize-a-virtual-environment)
    - [Install Dependencies](#install-dependencies)
    - [Set up Environment Variables for Backend](#set-up-environment-variables-for-backend)
    - [Set up Environment Variables for Frontend](#set-up-environment-variables-for-frontend)
    - [Adjust Docker Compose Configuration](#adjust-docker-compose-configuration)
  - [Run the Application](#run-the-application)
  - [Using the Application](#using-the-application)
  - [Database Schema](#database-schema)
    - [ERD](#erd)
    - [Badges](#badges)
    - [Accounts](#accounts)
    - [Trainers](#trainers)
    - [Pokedex](#pokedex)
    - [Special Moves](#special-moves)
    - [Armor](#armor)
    - [Helms](#helms)
    - [Boots](#boots)
    - [Weapons](#weapons)
    - [Pokemon](#pokemon)
    - [Trainer Party](#trainer-party)
    - [PokeBalls](#pokeballs)
  - [Internal API Documentation](#internal-api-documentation)
  - [API Index](#api-index)
    - [Overview](#overview)
    - [Purpose](#purpose)
    - [Authentication](#authentication)
  - [User Authentication Endpoints](#user-authentication-endpoints)
    - [Create Account](#create-account)
      - [Example Request](#example-request)
      - [Example Responses](#example-responses)
    - [Check User Status](#check-user-status)
      - [Example Response](#example-response)
    - [Login](#login)
      - [Example Request](#example-request-1)
      - [Example Response](#example-response-1)
    - [Logout](#logout)
      - [Example Response](#example-response-2)
  - [PokeBall Endpoint](#pokeball-endpoint)
    - [Get Specific PokeBall](#get-specific-pokeball)
      - [Example Request](#example-request-2)
      - [Example Response](#example-response-3)
  - [Pokemon Endpoints](#pokemon-endpoints)
    - [Get a Random Pokemon](#get-a-random-pokemon)
      - [Example Response](#example-response-4)
    - [Attempt to Catch a Pokemon](#attempt-to-catch-a-pokemon)
      - [Example Request](#example-request-3)
      - [Example Response](#example-response-5)
    - [Add Pokemon to User Party](#add-pokemon-to-user-party)
      - [Example Response](#example-response-6)
    - [Delete Specific Pokemon](#delete-specific-pokemon)
      - [Example Responses](#example-responses-1)
    - [Change Order of Pokemon in Party](#change-order-of-pokemon-in-party)
      - [Example Request](#example-request-4)
      - [Example Responses](#example-responses-2)
  - [Trainer Endpoints](#trainer-endpoints)
    - [Get Profile Page](#get-profile-page)
      - [Example Responses](#example-responses-3)
    - [Get Account Page](#get-account-page)
      - [Example Responses](#example-responses-4)
    - [Change Trainer Currency](#change-trainer-currency)
      - [Example Request](#example-request-5)
      - [Example Responses](#example-responses-5)
  - [External API Section](#external-api-section)
      - [Example Response](#example-response-7)
      - [Example Request](#example-request-6)
      - [Example Response](#example-response-8)

### About the project

[Back to Top](#index)

Module3 Project Gamma is a web application that allows users to play a Pokemon Safari-like minigame. Future features are planned to allow for Pokemon equipment, multiplayer, and earning of Pokemon levels and in-game currency. This project is built using a combination of Python for the backend, FastAPI for the API, with Tailwind-CSS and Vite-React for the frontend.

### Backend Overview

[Back to Top](#index)

The backend of Project Gamma is powered by FastAPI, a modern, fast web framework for building APIs with Python. It handles the game logic, user authentication, and interacts with a PostgreSQL database to store game data.
For more details, you can explore the various sections of the API documentation provided below.

### Frontend Overview

[Back to Top](#index)

The frontend utilizes Vite and React to create an interactive and dynamic user interface. Tailwind CSS is employed for styling, providing a sleek and responsive design. The frontend communicates with the backend API to fetch and display game-related information.

## Installation

Below is the installation guide to install the web-application to a local development environment using Docker.

### Prerequisites

[Back to Top](#index)

Make sure you have Docker, Git, and Node.js (version 20.10.7 or above)

### Getting Started

[Back to Top](#index)

Follow these steps to get a copy of the project up and running on your local machine for development and testing purposes.

### Clone the Repository

[Back to Top](#index)

First, open your terminal or command prompt and navigate to the directory where you want to clone the repository. Then run the following command:

```bash
git clone https://gitlab.com/ram_rod/project-gamma.git
```

This will clone the git repository and create a new directory labeled project-gamma.

### Optional: Initialize a Virtual Environment

[Back to Top](#index)

Initializing a virtual environment is optional but recommended to manage dependencies and keep your project isolated. Follow these steps to set up a virtual environment:

Navigate to the project directory:

```bash
cd project-gamma
```

Create a virtual environment. Replace "venvname" with your desired virtual environment name, in both all of the following commands:

```bash
python -m venv ./venvname
```

Activate the virtual environment:

On Windows:

```bash
./venvname/Scripts/activate
```

On macOS/Linux:

```bash
source venvname/bin/activate
```

When you are finished working on the project simply run:

```bash
deactivate
```

### Install Dependencies

[Back to Top](#index)

After cloning the repository, navigate into the project directory:

Example: cd project-gamma

Then, in your cmd prompt or terminal, run the command:

```bash
pip install -r api/requirements.txt
```

### Set up Environment Variables for Backend

[Back to Top](#index)

Create a `.env` file in the root directory of the project and add the necessary environment variables.

Required variables:

```bash
POSTGRES_USER=YOUR DESIRED POSTGRES USER
POSTGRES_PASSWORD=YOUR DESIRED POSTGRES PASS
PGADMIN_DEFAULT_EMAIL=YOURADMINEMAIL@ADMIN.COM
PGADMIN_DEFAULT_PASSWORD=YOURADMINPASSWORD
SIGNING_KEY=CREATE A UNIQUE SIGNING KEY
DEV_ENV=True
```

### Set up Environment Variables for Frontend

[Back to Top](#index)

Create a `.env` file in the ghi directory of the project and add the necessary environment variables.

```bash
VITE_API_HOST=http://localhost:8000
```

### Adjust Docker Compose Configuration

[Back to Top](#index)

Before running the Docker Compose command, make sure to review and adjust the configuration in the `docker-compose.yaml` file based on your preferences and requirements. Open the `docker-compose.yaml` file in a text editor and look for the following sections:

Adjust the ports as to your preferences.

```bash
services:
fastapi: # ... other configurations ...

        volumes:
            - ./api:/app
        ports:
            - 8000:8000

    ghi:
        # ... other configurations ...

        volumes:
            - ./ghi:/app
        ports:
            - 5173:5173


    postgres:
        # ... other configurations ...

        volumes:
            - poke_db:/var/lib/postgresql/data
        ports:
            - 5432:5432


    pgadmin:
        # ... other configurations ...

        volumes:
            - pgadmin_db:/var/lib/pgadmin
        ports:
            - 8082:80

```

## Run the Application

[Back to Top](#index)

Open docker before running the below commands. Failing to do so will result in an error, as docker would not be open to run the commands.

This next step covers creating docker volumes, images, and containers. In the docker-compose.yaml, we have set it so
these will automatically run on the command below:

```bash
docker compose up
```

IMPORTANT -

| **System Logs**                                  | **Description**                                                  |
| ------------------------------------------------ | ---------------------------------------------------------------- |
| 1. **poke_db TIMESTAMP LOG:**                    | - **poke_db:** Contains logs related to the PostgreSQL database. |
| Database system is ready to accept connections.  |                                                                  |
|                                                  |                                                                  |
| 2. **project-gamma-fastapi-1 INFO:**             | - **project-gamma-fastapi-1:** Logs from the FastAPI backend.    |
| Application startup complete.                    |                                                                  |
|                                                  |                                                                  |
| 3. **project-gamma-pgadmin-1 TIMESTAMP [INFO]:** | - **project-gamma-pgadmin-1:** Logs related to pgAdmin access.   |
| Booting worker with pid: XX.                     |                                                                  |
|                                                  |                                                                  |
| 4. **project-gamma-ghi-1 VITE vX.X.X ready:**    | - **project-gamma-ghi-1:** Logs from the VITE frontend.          |
| VITE frontend is ready.                          |                                                                  |

## Using the Application

[Back to Top](#index)

At this point, you should have access to the backend and the frontend of the application using the following urls, assuming you did not change the ports above. If you have, please adjust the urls below to match.

pgAdmin: http://localhost:8082/

FastAPI SwaggerUI: http://localhost:8000/docs

Vite + React Frontend: http://localhost:5173

[Back to Top](#index)

## Database Schema

### ERD

![ERD](docs/ERD.png)

### Badges

[Top of database schema](#database-schema)

[Skip to API Documentation](#internal-api-documentation)

```bash
CREATE TABLE IF NOT EXISTS badges (
    id  SERIAL UNIQUE  NOT NULL,
    badge_name varchar(20)  NOT NULL,
    sprite_url varchar(200)  NOT NULL,
    CONSTRAINT pk_badges PRIMARY KEY (id)
)
```

### Accounts

[Top of database schema](#database-schema)

[Skip to API Documentation](#internal-api-documentation)

```bash
CREATE TABLE IF NOT EXISTS accounts (
    id  SERIAL UNIQUE  NOT NULL,
    username varchar(20) UNIQUE NOT NULL,
    hashed_password varchar(255)  NOT NULL,
    CONSTRAINT pk_accounts PRIMARY KEY (id)
)
```

### Trainers

[Top of database schema](#database-schema)

[Skip to API Documentation](#internal-api-documentation)

```bash
CREATE TABLE IF NOT EXISTS trainers (
    id SERIAL UNIQUE NOT NULL,
    name varchar(20) NOT NULL,
    currency float NOT NULL,
    badge_id int NULL,
    accounts_id int NULL,
    CONSTRAINT pk_trainers PRIMARY KEY (id),
    CONSTRAINT fk_trainers_badge_id
        FOREIGN KEY(badge_id)
        REFERENCES badges (id),
    CONSTRAINT fk_trainers_accountsid
        FOREIGN KEY (accounts_id)
        REFERENCES accounts (id)
)
```

### Pokedex

[Top of database schema](#database-schema)

[Skip to API Documentation](#internal-api-documentation)

```bash
CREATE TABLE IF NOT EXISTS pokedex (
    id  SERIAL UNIQUE NOT NULL,
    name varchar(20)   NOT NULL,
    lvl int  NOT NULL,
    hp int  NOT NULL,
    dfns int  NOT NULL,
    atk int  NOT NULL,
    spd int  NOT NULL,
    element varchar(20)  NOT NULL,
    caught boolean  NOT NULL,
    sprite_url_front varchar(200)  NOT NULL,
    sprite_url_back varchar(200)  NOT NULL,
    CONSTRAINT pk_pokedex PRIMARY KEY (id)
)
```

### Special Moves

[Top of database schema](#database-schema)

[Skip to API Documentation](#internal-api-documentation)

```bash
CREATE TABLE IF NOT EXISTS special_moves (
    id  SERIAL  UNIQUE NOT NULL,
    name varchar(20)  NOT NULL,
    element varchar(20)  NOT NULL,
    dmg int  NOT NULL,
    CONSTRAINT pk_special_moves PRIMARY KEY (id)
)
```

### Armor

[Top of database schema](#database-schema)

[Skip to API Documentation](#internal-api-documentation)

```bash
CREATE TABLE IF NOT EXISTS armor (
    id  SERIAL  UNIQUE  NOT NULL,
    name varchar(20)  NOT NULL,
    def int  NOT NULL,
    durability int  NOT NULL,
    max_durability int  NOT NULL,
    CONSTRAINT pk_armor PRIMARY KEY (id)
)
```

### Helms

[Top of database schema](#database-schema)

[Skip to API Documentation](#internal-api-documentation)

```bash
CREATE TABLE IF NOT EXISTS helms (
    id  SERIAL  UNIQUE  NOT NULL,
    name varchar(20)  NOT NULL,
    def int  NOT NULL,
    durability int  NOT NULL,
    max_durability int  NOT NULL,
    CONSTRAINT pk_helm PRIMARY KEY (id)
)
```

### Boots

[Top of database schema](#database-schema)

[Skip to API Documentation](#internal-api-documentation)

```bash
CREATE TABLE IF NOT EXISTS boots (
    id  SERIAL  UNIQUE  NOT NULL,
    name varchar(20)  NOT NULL,
    def int  NOT NULL,
    durability int  NOT NULL,
    max_durability int  NOT NULL,
    CONSTRAINT pk_boots PRIMARY KEY (id)
)
```

### Weapons

[Top of database schema](#database-schema)

[Skip to API Documentation](#internal-api-documentation)

```bash
CREATE TABLE IF NOT EXISTS weapons (
    id  SERIAL  UNIQUE  NOT NULL,
    name varchar(20)  NOT NULL,
    atk int  NOT NULL,
    durability int  NOT NULL,
    max_durability int  NOT NULL,
    CONSTRAINT pk_weapon PRIMARY KEY (id)
)
```

### Pokemon

[Top of database schema](#database-schema)

[Skip to API Documentation](#internal-api-documentation)

```bash
CREATE TABLE IF NOT EXISTS poke_instances (
    poke_instance_id  SERIAL  UNIQUE  NOT NULL,
    pokedex_id int NOT NULL,
    name varchar(20)  NOT NULL,
    lvl int  NOT NULL,
    hp int  NOT NULL,
    dfns int  NOT NULL,
    atk int  NOT NULL,
    spd int  NOT NULL,
    element varchar(20)  NOT NULL,
    caught boolean  NOT NULL,
    sprite_url_front varchar(200)  NOT NULL,
    sprite_url_back varchar(200)  NOT NULL,
    helm_id int  NULL,
    chest_id int  NULL,
    boots_id int  NULL,
    weapons_id int  NULL,
    special_moves_id int  NULL,
    wins int  NULL,
    CONSTRAINT pk_poke_instances PRIMARY KEY (poke_instance_id),
    CONSTRAINT fk_poke_instances_pokedex_id
        FOREIGN KEY(pokedex_id)
        REFERENCES pokedex (id),
    CONSTRAINT fk_poke_instances_helm_id
        FOREIGN KEY(helm_id)
        REFERENCES helms (id),
    CONSTRAINT fk_poke_instances_chest_id
        FOREIGN KEY(chest_id)
        REFERENCES armor (id),
    CONSTRAINT fk_poke_instances_boots_id
        FOREIGN KEY(boots_id)
        REFERENCES boots (id),
    CONSTRAINT fk_poke_instances_weapons_id
        FOREIGN KEY(weapons_id)
        REFERENCES weapons (id),
    CONSTRAINT fk_poke_instances_special_moves_id
        FOREIGN KEY(special_moves_id)
        REFERENCES special_moves (id)
)
```

### Trainer Party

[Top of database schema](#database-schema)

[Skip to API Documentation](#internal-api-documentation)

```bash
CREATE TABLE IF NOT EXISTS mons_party (
    id  SERIAL UNIQUE NOT NULL,
    trainers_id INT NOT NULL,
    poke_instance_id INT UNIQUE NOT NULL,
    battle_order INT  NOT NULL,
    CONSTRAINT pk_mons_party PRIMARY KEY (id),
    CONSTRAINT fk_mons_party_trainers_id
        FOREIGN KEY(trainers_id)
        REFERENCES accounts (id),
    CONSTRAINT fk_mons_party_poke_instance_id
        FOREIGN KEY(poke_instance_id)
        REFERENCES poke_instances (poke_instance_id)
        ON DELETE CASCADE
)
```

### PokeBalls

[Top of database schema](#database-schema)

[Skip to API Documentation](#internal-api-documentation)

```bash
CREATE TABLE IF NOT EXISTS balls (
    id  SERIAL  UNIQUE  NOT NULL,
    ball_type varchar(20)  NOT NULL,
    cost float  NOT NULL,
    catch_rate float  NOT NULL,
    CONSTRAINT pk_balls PRIMARY KEY (id)
)
```

[Back to Top](#index)

## Internal API Documentation

## API Index

1. [User Authentication Endpoints](#user-authentication-endpoints)

    - [Create Account](#create-account)
    - [Check User Status](#check-user-status)
    - [Login](#login)
    - [Logout](#logout)

2. [PokeBall Endpoint](#pokeball-endpoint)

    - [Get Specific PokeBall](#get-specific-pokeball)

3. [Pokemon Endpoints](#pokemon-endpoints)

    - [Get Random Pokemon](#get-random-pokemon)
    - [Attempt to Catch a Pokemon](#attempt-to-catch-a-pokemon)
    - [Add Pokemon to User Party](#add-pokemon-to-user-party)
    - [Delete Specific Pokemon](#delete-specific-pokemon)
    - [Change Order of Pokemon in Party](#change-order-of-pokemon-in-party)

4. [Trainer Endpoints](#trainer-endpoints)
    - [Get Profile Page](#get-profile-page)
    - [Get Account Page](#get-account-page)
    - [Change Trainer Currency](#change-trainer-currency)

### Overview

Welcome to the internal API documentation for our project! This documentation provides comprehensive details about the endpoints and functionality of our backend services, designed to support various features related to user authentication, Pokémon interactions, and trainer management.

### Purpose

The primary goal of our internal API is to facilitate communication between the backend services and any associated frontend applications. It powers features such as user authentication, Pokémon encounters, capturing, and managing a trainer's profile.

### Authentication

To directly access the internal APIs, navigate to your FastAPI SwaggerUI URL (default is http://localhost:8000/docs)

## User Authentication Endpoints

This section contains the API documentation regarding user authentication

| Action            | Method | URL             | Parameters                     |
| ----------------- | :----: | --------------- | ------------------------------ |
| Create Account    |  POST  | `/api/accounts` | Username (str), Password (str) |
| Check User Status |  GET   | `/token`        | Current token (automatic)      |
| Login             |  POST  | `/token`        | Username (str), Password (str) |
| Logout            | DELETE | `/token`        | Current token (automatic)      |

### Create Account

[Back to Top](#internal-api-documentation-overview)

[API Index](#api-index)

This API endpoint creates a new account, using the input username(str) and password(str). The backend checks for a duplicate username, and will raise an error if a username is duplicated. The backend also ensures The frontend requires a password and username to be passed into the form.

```bash
URL: /api/accounts
Method: POST
```

#### Example Request

Valid Request:

```bash
{
  "username": "exampleusername",
  "password": "examplepassword"
}
```

Invalid Request:

```bash
{
  "username": "useralreadyexists",
  "password": "examplepassword"
}
```

#### Example Responses

Successful Response (200):

```bash
{
    "access_token": "longstringrepresentingtoken",
    "token_type": "Bearer",
    "account": {
        "id": account_id,
        "username": "exampleusername"
    },
    "type": "Bearer"
}
```

Error: Bad Request (400):

Both an already existing username and a null password will trigger the response below:

```bash
{
  "detail": "Cannot create an account with those credentials"
}
```

### Check User Status

[Back to Top](#internal-api-documentation-overview)

[API Index](#api-index)

This API endpoint checks to see if a token exists associated with the current user. The token is generated on login/signup, and deleting the token logs the user out.
This endpoint requires user status to be successful to access.

```bash
URL: /token
Method: GET
```

#### Example Response

Response if (token exists)
Successful Response

```bash
{
    "access_token": "longstringrepresentingtoken",
    "token_type": "Bearer",
    "account": {
        "id": account_id,
        "username": "exampleusername"
    },
    "type": "Bearer"
}
```

Response if (token does not exist)

```bash
null
```

### Login

[Back to Top](#internal-api-documentation-overview)

[API Index](#api-index)

This API endpoint checks to see if the username and password match what is on the database for the username, and then generates a token if it matches.

```bash
URL: /token
Method: Post
Content-Type: application/x-www-form-urlencoded
```

#### Example Request

Valid Request:

```
username: exampleusername
password: examplepassword
```

Invalid Request:

```
username: invaliduser
password: incorrectpassword
```

#### Example Response

Successful Response (200):

```bash
{
  "access_token": "superlongstringrepresentingtoken",
  "token_type": "Bearer"
}
```

Error: Unauthorized (Incorrect username or password):

```bash
{
  "detail": "Incorrect username or password"
}
```

### Logout

```bash
URL: /token
Method: DELETE
```

This API endpoint deletes the current user's token.

#### Example Response

Successful Response(200):

```bash
true
```

## PokeBall Endpoint

This section contains the API documentation regarding PokeBalls

| Action                  | Method | URL                     | Parameters      |
| ----------------------- | :----: | ----------------------- | --------------- |
| Get a Specific PokeBall |  GET   | `/api/balls{ball_type}` | ball_type (str) |

### Get Specific PokeBall

[Back to Top](#internal-api-documentation-overview)

[API Index](#api-index)

This API was meant to be used to access specific ball information from the ball table by type, however this function was deprecated. Logic for pulling balls by type is now on the frontend.

```bash
URL: /api/balls/{ball_type}
METHOD: GET
```

#### Example Request

Valid Request:

```bash
"SafariBall"
```

Invalid Request:

```bash
"InvalidBall"
```

#### Example Response

Successful Response (200):

```bash
"SafariBall"
```

Response (404) Error: Not Found:

```bash
{
  "detail": "Ball not found"
}
```

## Pokemon Endpoints

[Back to Top](#internal-api-documentation-overview)

[API Index](#api-index)

All endpoints in the Pokemon section require user status to be successful to access.

| Action                           | Method | URL                                       | Parameters                               |
| -------------------------------- | :----: | ----------------------------------------- | ---------------------------------------- |
| Get Random Pokemon               |  GET   | `/api/pokemon/encounter`                  |                                          |
| Attempt to Catch a Pokemon       |  POST  | `/api/pokemon/attempt-catch`              | Token, pokemon_id (int), ball_type (str) |
| Add Pokemon to User Party        |  POST  | `/api/pokemon/party/add-pokemon-to-party` | Token, poke_instance_id (int)            |
| Delete Specific Pokemon          | DELETE | `/api/pokemon{poke_instance_id}`          | Token, Poke_instance_id (int)            |
| Change Order of Pokemon in Party |  PUT   | `/api/pokemon/party/update-battle-order`  | Token,Pokemon Order (array(int))         |

### Get a Random Pokemon

[Back to Top](#internal-api-documentation-overview)

[API Index](#api-index)

This API endpoint can be used to get a random pokemon based on backend logic. It rolls between a range of 1-151 and displays the details of the pokemon.

```bash
URL: /api/pokemon/encounter
METHOD: GET
```

#### Example Response

```bash
{
  "id": 125,
  "name": "electabuzz",
  "lvl": 1,
  "hp": 65,
  "dfns": 57,
  "atk": 83,
  "spd": 105,
  "element": "electric",
  "sprite_url_front": "https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/showdown/125.gif",
  "sprite_url_back": "https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/showdown/back/125.gif"
}
```

### Attempt to Catch a Pokemon

[Back to Top](#internal-api-documentation-overview)

[API Index](#api-index)

This API endpoint is used to take in a pokemon_id, and a ball_type, depending on other logic to determine how a number generated compares to a capture chance on the ball by type. If the pokemon is captured, this endpoint adds the pokemon to the poke_instance table. The information generated for the poke_instance is pulled from the pokedex table.

```bash
URL: /api/pokemon/attempt-catch
METHOD: POST
```

#### Example Request

```bash
{
  "pokemon_id": 1,
  "ball_type": "PokeBall"
}
```

#### Example Response

Caught Pokemon:

```bash
{
  "poke_instance_id": 5,
  "name": "bulbasaur",
  "lvl": 1,
  "hp": 45,
  "dfns": 49,
  "atk": 49,
  "spd": 45,
  "element": "grass",
  "caught": true,
  "sprite_url_front": "https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/showdown/1.gif",
  "sprite_url_back": "https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/showdown/back/1.gif",
  "helm_id": null,
  "chest_id": null,
  "boots_id": null,
  "weapons_id": null,
  "special_moves_id": null,
  "wins": 0
}
```

Failed to catch Pokemon:

```bash
{
  "detail": "Failed to catch Pokemon"
}
```

### Add Pokemon to User Party

[Back to Top](#internal-api-documentation-overview)

[API Index](#api-index)

This API endpoint is used to add a trainer_id to the poke_instance, thereby associating the Pokemon with the trainer through use of the Mons_Party table. If the trainer has less than 3 Pokemon, the Pokemon is associated with the trainer. Else, the Pokemon is not. The logic increments the battle_order if the battle_order is < 3. On the frontend this is automatically handled with the capture page.

```bash
URL: /api/pokemon/party/add-pokemon-to-party
METHOD: POST
BODY: 1
## BODY is poke_instance_id to be associated.
```

#### Example Response

Successful Response (200)
Pokemon is Caught

```bash
{
  "id": 1,
  "trainers_id": 1,
  "poke_instance_id": 1,
  "battle_order": 1
}
```

Party is Full Response (202)

```bash
{
  "detail": "The party is already full. Cannot add more Pokemon."
}
```

### Delete Specific Pokemon

This API endpoint is used to DELETE a Pokemon from the poke_instance table by use of the poke_instance_id. This will remove all associations with the Pokemon as well. This is handled on the frontend by a delete button

```bash
URL: /api/pokemon{poke_instance_id}
METHOD: DELETE
```

#### Example Responses

If Pokemon is Deleted:

```bash
{
  "message": "You Old Yellard that one!"
}
```

If Pokemon Does not Exist:

Error 404: Not Found

```bash
{
  "detail": "Pokémon instance not found."
}
```

### Change Order of Pokemon in Party

[Back to Top](#internal-api-documentation-overview)

[API Index](#api-index)

This API endpoint can be used to change the battle_order attribute attached to the 3 pokemon in the trainer's party. Currently there is no frontend implementation of this feature.
Future implementation of this is planned specifically for trainer battles.

```bash
URL: /api/pokemon/party/update-battle-order
METHOD: PUT
```

#### Example Request

Valid Request:
(array of ints, 1, 2, 3 to be assigned to the pokemon from first to last in current battle_order)

```bash
[
  2, 1, 3
]
```

#### Example Responses

Successful Response (200):

```bash
{
  "message": "Successfully updated battle order Example Trainer!"
}
```

## Trainer Endpoints

The API endpoints in this section regard the trainer, otherwise known as the user, account, or player. They all require user authentication success.

| Action                  | Method | URL                          | Parameters                 |
| ----------------------- | :----: | ---------------------------- | -------------------------- |
| Get Profile Page        |  GET   | `/api/trainers/profile/mine` | Token                      |
| Get Account Page        |  GET   | `/api/trainers/account/mine` | Token                      |
| Change Trainer Currency |  PUT   | `/api/trainers/currency`     | Currency to be added (int) |

### Get Profile Page

[Back to Top](#internal-api-documentation-overview)

[API Index](#api-index)

This API endpoint will return all the information pertaining to the trainer_id by a query joining the mons_party table, the poke_instances table, and the trainer table.

```bash
URL: /api/trainers/profile/mine
METHOD: GET
```

#### Example Responses

Successful Response with 2 Pokemon (200):

```bash
[
  {
    "trainers_id": 1,
    "trainer_name": "testUser1",
    "badge_id": 1,
    "currency": 120,
    "battle_order": 2,
    "poke_instance_id": 6,
    "pokemon_name": "bulbasaur",
    "lvl": 1,
    "hp": 45,
    "dfns": 49,
    "atk": 49,
    "spd": 45,
    "element": "grass",
    "sprite_url_front": "https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/showdown/1.gif",
    "sprite_url_back": "https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/showdown/back/1.gif"
  },
  {
    "trainers_id": 1,
    "trainer_name": "testUser1",
    "badge_id": 1,
    "currency": 120,
    "battle_order": 3,
    "poke_instance_id": 4,
    "pokemon_name": "charmander",
    "lvl": 1,
    "hp": 39,
    "dfns": 43,
    "atk": 52,
    "spd": 65,
    "element": "fire",
    "sprite_url_front": "https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/showdown/4.gif",
    "sprite_url_back": "https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/showdown/back/4.gif"
  }
]
```

Successful Response with 0 Pokemon (200):

```bash
[
  {
    "trainers_id": 1,
    "trainer_name": "testUser1",
    "badge_id": 1,
    "currency": 120,
    "battle_order": null,
    "poke_instance_id": null,
    "pokemon_name": null,
    "lvl": null,
    "hp": null,
    "dfns": null,
    "atk": null,
    "spd": null,
    "element": null,
    "sprite_url_front": null,
    "sprite_url_back": null
  }
]
```

### Get Account Page

[Back to Top](#internal-api-documentation-overview)

[API Index](#api-index)

This API endpoint will return the trainer username and trainer id. This was to be implemented into an account page but was deemed unecessary for current implementation.

```bash
URL: /api/trainers/account/mine
METHOD: GET
```

#### Example Responses

Successful Response (200):

```bash
{
  "id": 1,
  "username": "testUser1"
}
```

If user is not logged in:
Error: Unauthorized

```bash
{
  "detail": "Invalid token"
}
```

### Change Trainer Currency

[Back to Top](#internal-api-documentation-overview)

[API Index](#api-index)

This API endpoint is used to change the trainer currency. It adds the input number to the trainer currency, and can handle negatives.

```bash
URL: /api/trainers/currency
METHOD: PUT
```

#### Example Request

```bash
{
  "currency": 123451
}
```

#### Example Responses

Successful Response:

```bash
{
  "message": "Currency updated successfully."
}
```

If user is not logged in:
Error: Unauthorized(401)

```bash
{
  "detail": "Invalid token"
}
```

## External API Section

This application makes 151 third party API calls upon start up and then writes them to the Pokedex table in the postgres database for later use.
No authentication or API keys are required, this runs on server startup. The details below for each pokemon are added to the pokedex table/

#### Example Response

The application makes an API call to https://pokeapi.co/api/v2/pokemon/, one for each of the first 151 pokemon and gets the relevant details.

#### Example Request

We grab specific data from the API:

```
{
  "name": "dragonair",
  "stats": [
    {
      "base_stat": 61
    },
    {
      "base_stat": 84
    },
    {
      "base_stat": 65
    },
    {
      "base_stat": 70
    },
  ],
  "types": {
    "types": [
      {
        "type": {
          "name": "dragon"
        }
      }
    ]
  }
  "sprites": {
    "other": {
      "showdown": {
        "back_default": "https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/showdown/back/148.gif",
        "front_default": "https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/showdown/148.gif",
      }
    }
  }
}
```

#### Example Response

Upon successful call we receive a status code 200 for each pokemon:

```
<Response [200 OK]>
```
