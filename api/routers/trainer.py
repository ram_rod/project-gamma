from fastapi import APIRouter, Depends, HTTPException, status
from authenticator import authenticator
from models import AccountOut, UpdateTrainerCurrency
from queries.trainers import TrainersQueries


router = APIRouter()


@router.put("/api/trainers/currency", status_code=status.HTTP_200_OK)
async def update_currency(
    currency_data: UpdateTrainerCurrency,
    account_data: dict = Depends(authenticator.get_current_account_data),
    queries: TrainersQueries = Depends(),
):
    trainer_id = account_data["id"]
    success = await queries.update_trainer_currency(
        trainer_id, currency_data.currency
    )
    if not success:
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND, detail="Trainer not found."
        )
    return {"message": "Currency updated successfully."}


@router.get("/api/trainers/account/mine", response_model=AccountOut)
async def get_account_info(
    account_data: dict = Depends(authenticator.get_current_account_data),
    trainer_queries: TrainersQueries = Depends(),
):
    account_id = account_data["id"]
    request = await trainer_queries.get_trainer_info(account_id)
    if request is None:
        raise HTTPException(status_code=404, detail="Account not found")
    return request
