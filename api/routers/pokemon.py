from fastapi import APIRouter, Depends, HTTPException, status
from authenticator import authenticator
from models import PokedexToPokeinstance, CatchRequest, RandomEncounter
from queries.utility import get_random_pokemon
from queries.pokemon import PokemonQueries
from queries.monsparty import MonsPartyQueries
import random


router = APIRouter()


@router.delete(
    "/api/pokemon/{poke_instance_id}", status_code=status.HTTP_200_OK
)
async def delete_pokemon_instance(
    poke_instance_id: int,
    account_data: dict = Depends(authenticator.get_current_account_data),
    queries: PokemonQueries = Depends(),
):
    try:
        await queries.remove_poke_instance(poke_instance_id)
        return {"message": "You Old Yellard that one!"}

    except HTTPException as http_exc:
        raise http_exc


@router.get("/api/pokemon/encounter", response_model=RandomEncounter)
async def encounter_pokemon(
    account_data: dict = Depends(authenticator.get_current_account_data),
):
    wild_pokemon = await get_random_pokemon()
    return wild_pokemon


@router.post(
    "/api/pokemon/attempt-catch", response_model=PokedexToPokeinstance
)
async def attempt_catch_pokemon(
    catch_request: CatchRequest,
    account_data: dict = Depends(authenticator.get_current_account_data),
    queries: PokemonQueries = Depends(),
    mparty: MonsPartyQueries = Depends(),
):
    # grab the id and ball type from the request
    trainer_id = account_data["id"]
    pokemon_id = catch_request.pokemon_id
    ball_type = catch_request.ball_type
    # Get the catch rate of the selected ball
    catch_rate = await queries.get_ball_catch_rate(ball_type)
    # Attempt to catch
    if random.random() <= catch_rate:
        # Catch is successful, insert poke instance
        poke_instance = await queries.insert_poke_instance(
            pokedex_id=pokemon_id
        )
        add_to_party = await mparty.add_pokeinstance_to_party(
            trainer_id=trainer_id,
            poke_instance_id=poke_instance.poke_instance_id,
        )
        return poke_instance
    else:
        # Catch failed
        raise HTTPException(
            status.HTTP_202_ACCEPTED, detail="Failed to catch Pokemon"
        )
