from fastapi import APIRouter, Depends, HTTPException
from authenticator import authenticator
from models import ProfileInfo
from queries.profilepage import ProfilePageQueries
from typing import List

router = APIRouter()


@router.get("/api/trainers/profile/mine", response_model=List[ProfileInfo])
async def get_profile_page(
    account_data: dict = Depends(authenticator.get_current_account_data),
    profile_queries: ProfilePageQueries = Depends(),
):
    trainer_id = account_data["id"]
    try:
        profile_data = await profile_queries.get_profile_data(trainer_id)
        return profile_data
    except HTTPException as e:
        if e.status_code == 500:
            return [
                {
                    "message": "You do not have any Pokémon. "
                    "Please click the encounter page."
                }
            ]
        raise e
