import os
from contextlib import asynccontextmanager
from psycopg_pool import AsyncConnectionPool, ConnectionPool


DATABASE_URL = os.environ.get("DATABASE_URL")


pool = ConnectionPool(conninfo=DATABASE_URL)


async def get_async_pool():
    async with AsyncConnectionPool(
        conninfo=os.environ["DATABASE_URL"]
    ) as asyncpool:
        yield asyncpool


@asynccontextmanager
async def get_api_connection():
    async for pool in get_async_pool():
        async with pool.connection() as conn:
            yield conn


async def is_pokedex_populated(conn):
    async with conn.cursor() as cur:
        await cur.execute("SELECT 1 FROM pokedex LIMIT 1;")
        return await cur.fetchone() is not None


async def close_async_pool():
    pool.close()
