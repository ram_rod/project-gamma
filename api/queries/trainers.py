from queries.client import get_api_connection
from models import Trainers
from psycopg.rows import dict_row
from queries.errors import AccountNotFound


class TrainersQueries:
    async def update_trainer_currency(
        self, trainer_id: int, currency_adjustment: float
    ):
        try:
            async with get_api_connection() as conn:
                async with conn.cursor(row_factory=dict_row) as cur:
                    # Fetch the current currency value
                    fetch_query = """
                        SELECT currency
                        FROM trainers
                        WHERE id = %s;
                    """
                    await cur.execute(fetch_query, (trainer_id,))
                    result = await cur.fetchone()
                    if not result:
                        return False
                    # Trainer not found

                    current_currency = result["currency"]

                    # Calculate the new currency value
                    new_currency = current_currency + currency_adjustment

                    # Update the trainer's currency
                    update_query = """
                        UPDATE trainers
                        SET currency = %s
                        WHERE id = %s;
                    """
                    await cur.execute(update_query, (new_currency, trainer_id))
                    return True
        except Exception as e:
            raise e

    async def create_trainer(self, trainer_data: Trainers):
        try:
            name = trainer_data.name
            currency = 10000
            badge_id = trainer_data.badge_id
            accounts_id = trainer_data.accounts_id

            async with get_api_connection() as conn:
                async with conn.cursor(row_factory=dict_row) as cur:
                    query = """
                            INSERT INTO trainers (
                                name,
                                currency,
                                badge_id,
                                accounts_id)
                            VALUES (%s, %s, %s, %s)
                            RETURNING id;
                            """
                    await cur.execute(
                        query, (name, currency, badge_id, accounts_id)
                    )
                    trainer_id = await cur.fetchone()
                    return trainer_id
        except AccountNotFound:
            raise
        except Exception:
            raise

    async def get_trainer_info(self, account_id: int):
        async with get_api_connection() as conn:
            async with conn.cursor(row_factory=dict_row) as cur:
                query = """
                            SELECT
                            id,
                            username
                            FROM accounts
                            WHERE id = %s
                            """
                await cur.execute(query, (account_id,))
                trainer_info = await cur.fetchone()
                return trainer_info
