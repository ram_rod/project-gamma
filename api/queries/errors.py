class DuplicateAccountError(ValueError):
    pass


class AccountNotFound(Exception):
    pass


class DatabaseError(Exception):
    pass
