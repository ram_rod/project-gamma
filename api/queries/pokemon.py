from queries.client import get_api_connection
from models import PokedexToPokeinstance
from psycopg.rows import dict_row
from fastapi import HTTPException, status, Response


"""
This function passes in the randomly generated pokedex ID from the
router and inserts the pokedex row into the poke_instances table following
the PokedexToPokeInstance pydantic model for validation.
The account id needs to come from trainer???
"""


class PokemonQueries:
    async def get_ball_catch_rate(self, ball_type: str) -> float:
        try:
            async with get_api_connection() as conn:
                async with conn.cursor(row_factory=dict_row) as cur:
                    query = """
                        SELECT catch_rate
                        FROM balls
                        WHERE ball_type = %s;
                    """
                    await cur.execute(query, (ball_type,))
                    ball_info = await cur.fetchone()

                    if ball_info:
                        return ball_info["catch_rate"]
                    else:
                        raise Exception(f"Ball type {ball_type} not found")
        except HTTPException as e:
            raise e

    async def insert_poke_instance(self, pokedex_id: int):
        try:
            async with get_api_connection() as conn:
                async with conn.cursor(row_factory=dict_row) as cur:
                    insert_query = """
                                INSERT INTO poke_instances (
                                    pokedex_id,
                                    name,
                                    lvl,
                                    hp,
                                    dfns,
                                    atk,
                                    spd,
                                    element,
                                    caught,
                                    sprite_url_front,
                                    sprite_url_back,
                                    helm_id,
                                    chest_id,
                                    boots_id,
                                    weapons_id,
                                    special_moves_id,
                                    wins
                                )
                                SELECT
                                    id,
                                    name,
                                    lvl,
                                    hp,
                                    dfns,
                                    atk,
                                    spd,
                                    element,
                                    true,
                                    sprite_url_front,
                                    sprite_url_back,
                                    NULL, -- helm_id
                                    NULL, -- chest_id
                                    NULL, -- boots_id
                                    NULL, -- weapons_id
                                    NULL, -- special_moves_id
                                    0     -- wins
                                FROM pokedex
                                WHERE id = %s
                                RETURNING *;
                            """
                    await cur.execute(insert_query, (pokedex_id,))
                    row = await cur.fetchone()
                    if row:
                        return PokedexToPokeinstance(**row)
                    else:
                        raise Exception(
                            f"Pokemon with ID {pokedex_id} not found"
                        )
        except Exception as e:
            raise e

    async def remove_poke_instance(self, poke_instance_id: int):
        try:
            async with get_api_connection() as conn:
                async with conn.cursor() as cur:
                    # Delete from poke_instances,
                    # //CASCADE\\ will handle mons_party
                    delete_query = """
                        DELETE FROM poke_instances
                        WHERE poke_instance_id = %s;
                    """
                    await cur.execute(delete_query, (poke_instance_id,))
                    deleted = cur.rowcount

                    if deleted == 0:
                        # No rows were deleted,
                        # meaning the Pokémon instance wasn't found
                        raise HTTPException(
                            status_code=status.HTTP_404_NOT_FOUND,
                            detail="Pokémon instance not found.",
                        )

                    # If deletion was successful, no need to return a body,
                    # just a success status code
                    return Response(status_code=status.HTTP_200_OK)

        except Exception as e:
            # Log or handle the specific exception
            raise e
