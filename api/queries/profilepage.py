from fastapi import HTTPException
from psycopg.rows import dict_row
from queries.client import get_api_connection


class ProfilePageQueries:
    async def get_profile_data(self, trainer_id: int):
        try:
            async with get_api_connection() as conn:
                async with conn.cursor(row_factory=dict_row) as cur:
                    query = """
                        SELECT
                            trainers.id as trainers_id,
                            trainers.name as trainer_name,
                            trainers.badge_id,
                            trainers.currency,
                            mons_party.battle_order,
                            poke_instances.poke_instance_id,
                            poke_instances.name as pokemon_name,
                            poke_instances.lvl,
                            poke_instances.hp,
                            poke_instances.dfns,
                            poke_instances.atk,
                            poke_instances.spd,
                            poke_instances.element,
                            poke_instances.sprite_url_front,
                            poke_instances.sprite_url_back
                        FROM
                            trainers
                        LEFT JOIN
                            mons_party ON trainers.id = mons_party.trainers_id
                        LEFT JOIN
                            poke_instances ON
                            mons_party.poke_instance_id =
                            poke_instances.poke_instance_id
                        WHERE
                            trainers.id = %s
                        ORDER BY
                            mons_party.battle_order
                    """
                    await cur.execute(query, (trainer_id,))
                    profile_data = await cur.fetchall()
                    # Process the data to handle NULL values
                    return profile_data
        except Exception as e:
            print(f"Error details: {e}")
            raise HTTPException(
                status_code=500,
                detail=f"An unexpected error occurred. Error: {str(e)}",
            ) from e
