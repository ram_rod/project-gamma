from queries.client import pool
from psycopg.rows import dict_row
import psycopg
from queries.errors import (
    DuplicateAccountError,
    DatabaseError,
)
from models import (
    AccountIn,
    AccountOutWithHash,
)


class AccountsQueries:
    def get(self, username: str) -> AccountOutWithHash:
        try:
            with pool.connection() as conn:
                with conn.cursor(row_factory=dict_row) as cur:
                    cur.execute(
                        """
            SELECT *
            FROM accounts
            WHERE "username" = %s
            """,
                        [
                            username,
                        ],
                    )
                    account = cur.fetchone()
                    if account is not None:
                        return AccountOutWithHash(**account)
                    else:
                        raise ValueError(
                            f"No account found with username: {username}"
                        )
        except Exception as error:
            print(f"Unexpected error: {error}")
            raise DatabaseError(
                "An error occurred while trying to retrieve the account"
            )

    def create(
        self, info: AccountIn, hashed_password: str
    ) -> AccountOutWithHash:

        with pool.connection() as conn:
            with conn.cursor(row_factory=dict_row) as cur:
                cur.execute(
                    """
                    SELECT * from accounts
                    WHERE username = %s
                    """, [info.username])
                if cur.fetchone():
                    raise DuplicateAccountError(
                        f"Cannot create account: {info.username} already exists"
                    )
                else:
                    cur.execute(
                        """
                        INSERT INTO accounts (username, hashed_password)
                        VALUES (%s, %s)
                        RETURNING id, username, hashed_password
                        """,
                        [info.username, hashed_password],
                    )
                    new_account = cur.fetchone()
                    if new_account is not None:
                        return AccountOutWithHash(
                            id=new_account["id"],
                            username=new_account["username"],
                            hashed_password=new_account["hashed_password"],
                        )
                    else:
                        raise DatabaseError("Account not created")
