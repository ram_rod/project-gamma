steps = [
    [  # Data Up migration
        """
        INSERT INTO balls (ball_type, cost, catch_rate)
        VALUES
            ('SafariBall', 0, 0.10),
            ('PokeBall', 5, 0.20),
            ('GreatBall', 10, 0.40),
            ('UltraBall', 25, 0.75),
            ('MasterBall', 60, 1.00);
        """,
        # Data Down migration
        """
        DELETE FROM balls WHERE ball_type IN (
            'SafariBall',
            'PokeBall',
            'GreatBall',
            'UltraBall',
            'MasterBall'
        );
        """,
    ]
]
