steps = [
    [  # Up migration
        """
        CREATE TABLE IF NOT EXISTS mons_party (
            id  SERIAL UNIQUE NOT NULL,
            trainers_id INT NOT NULL,
            poke_instance_id INT UNIQUE NOT NULL,
            battle_order INT  NOT NULL,
            CONSTRAINT pk_mons_party PRIMARY KEY (id),
            CONSTRAINT fk_mons_party_trainers_id
                FOREIGN KEY(trainers_id)
                REFERENCES accounts (id),
            CONSTRAINT fk_mons_party_poke_instance_id
                FOREIGN KEY(poke_instance_id)
                REFERENCES poke_instances (poke_instance_id)
                ON DELETE CASCADE
        );
        """,
        # Down migration
        """
        DROP TABLE IF EXISTS mons_party;
        """,
    ]
]
