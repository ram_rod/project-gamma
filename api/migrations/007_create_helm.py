steps = [
    [  # Up migration
        """
        CREATE TABLE IF NOT EXISTS helms (
            id  SERIAL  UNIQUE  NOT NULL,
            name varchar(20)  NOT NULL,
            def int  NOT NULL,
            durability int  NOT NULL,
            max_durability int  NOT NULL,
            CONSTRAINT pk_helm PRIMARY KEY (id)
        );
        """,
        # Down migration
        """
        DROP TABLE IF EXISTS helms;
        """,
    ]
]
