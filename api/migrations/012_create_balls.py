steps = [
    [  # Up migration
        """
        CREATE TABLE IF NOT EXISTS balls (
            id  SERIAL  UNIQUE  NOT NULL,
            ball_type varchar(20)  NOT NULL,
            cost float  NOT NULL,
            catch_rate float  NOT NULL,
            CONSTRAINT pk_balls PRIMARY KEY (id)
        );
        """,
        # Down migration
        """
        DROP TABLE IF EXISTS balls;
        """,
    ]
]
