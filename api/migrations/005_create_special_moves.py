steps = [
    [  # Up migration
        """
        CREATE TABLE IF NOT EXISTS special_moves (
            id  SERIAL  UNIQUE NOT NULL,
            name varchar(20)  NOT NULL,
            element varchar(20)  NOT NULL,
            dmg int  NOT NULL,
            CONSTRAINT pk_special_moves PRIMARY KEY (id)
        );
        """,
        # Down migration
        """
        DROP TABLE IF EXISTS special_moves;
        """,
    ]
]
