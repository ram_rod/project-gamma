from fastapi.testclient import TestClient
from fastapi import status
from authenticator import authenticator
from main import app
from queries.pokemon import PokemonQueries

# Initialize the TestClient with the FastAPI application
client = TestClient(app)


def mock_get_current_account_data():
    return {"id": 42, "username": "MilesDavis"}


# Mock the pokemonqueries.remove_poke_instance function
class MockPokemonQueries:
    async def remove_poke_instance(self, poke_instance_id: int):
        if poke_instance_id == 13:
            return status.HTTP_200_OK


# Unit test for the delete_pokemon_instance endpoint
def test_delete_pokemon_instance():
    # Arrange
    app.dependency_overrides[PokemonQueries] = MockPokemonQueries
    app.dependency_overrides[
        authenticator.get_current_account_data
    ] = mock_get_current_account_data

    # Act
    # Make a DELETE request to the endpoint
    response = client.delete("/api/pokemon/13")

    # Assert
    # Check that the status code is 200 (OK)
    assert response.status_code == 200
    # Check that the JSON response matches the expected output
    assert response.json() == {"message": "You Old Yellard that one!"}
