import { Button } from '@/components/ui/button'

function DeleteModal({ isOpen, onClose, onConfirmDelete, children }) {
    if (!isOpen) return null

    return (
        <div className="fixed top-[0] left-[0] w-full h-full bg-[rgba(0,_0,_0,_0.8)] flex items-center justify-center pl-24">
            <div className="border-[solid] border-[1px] border-[#8cd4d7] bg-background p-[20px] rounded-[5px] [box-shadow:0_0_10px_rgba(0,_0,_0,_0.2)] flex flex-col items-stretch gap-[10px]">
                {children}
                <div className="flex justify-between w-full">
                    <Button
                        className="bg-[linear-gradient(120deg,_#2c0000,_#f81b1b,_#140000)] rounded-[5px] whitespace-nowrap overflow-hidden overflow-ellipsis min-w-[120px] text-md w-[130px]"
                        onClick={onConfirmDelete}
                    >
                        Yes, release
                    </Button>
                    <Button variant="secondary" className="self-end mt-auto text-md rounded w-[130px]" onClick={onClose}>
                        Cancel
                    </Button>
                </div>
            </div>
        </div>
    )
}

export default DeleteModal
