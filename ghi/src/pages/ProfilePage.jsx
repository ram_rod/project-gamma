import { useState, useEffect } from 'react'
import TrainerCard from '../components/profile/TrainerCard'
import MonsPartyCard from '../components/profile/MonsPartyCard'
import { profileLoader } from '../../loaders'
import { deletePokemon } from '@/services/api/modServices'
import DeleteModal from '../components/profile/DeleteModal'

function ProfilePage() {
    const [profileData, setProfileData] = useState(null)
    const [isLoading, setIsLoading] = useState(true)
    const [error, setError] = useState(null)
    const [isModalOpen, setIsModalOpen] = useState(false)
    const [pendingPokeInstanceId, setPendingPokeInstanceId] = useState(null)

    useEffect(() => {
        const fetchData = async () => {
            try {
                const data = await profileLoader()
                setProfileData(data.profileInfo)
            } catch (err) {
                setError(err)
            } finally {
                setIsLoading(false)
            }
        }

        fetchData()
    }, [])


    const toggleModal = () => {
        setIsModalOpen(!isModalOpen)
    }

    const handleDeleteConfirmation = async () => {
        try {
            await deletePokemon(pendingPokeInstanceId)
            toggleModal()

            // Fetches the profile data
            const updatedData = await profileLoader()
            setProfileData(updatedData.profileInfo)
            setPendingPokeInstanceId(null)
        } catch (error) {
            console.error('Failed to delete Pokemon:', error)
            toggleModal()
        }
    }

    const handleDelete = (pokeInstanceId) => {
        setPendingPokeInstanceId(pokeInstanceId)
        toggleModal()
    }

    if (isLoading) {
        return <div>Loading...</div>
    }

    if (error) {
        return <div>Error: {error.message}</div>
    }

    if (!profileData || profileData.length === 0) {
        return (
            <div className="flex flex-col gap-4 ml-[125px] p-4">
                <div className="flex-grow">
                    <div className="flex flex-col items-center justify-center w-full mx-auto"></div>
                    <div>No profile data available.</div>
                </div>
            </div>
        )
    }

    return (
        <div className="flex flex-col gap-4 ml-[125px] p-4">
            <div className="flex-grow">
                <div className="flex flex-col items-center justify-center w-full mx-auto">
                    <div>
                        <h1>Profile:</h1>
                        <TrainerCard
                            trainer={{
                                trainer_name: profileData[0].trainer_name,
                                badge_id: profileData[0].badge_id,
                                currency: profileData[0].currency,
                            }}
                        />
                        <MonsPartyCard
                            pokemonInstances={profileData}
                            onDelete={handleDelete}
                        />
                        <DeleteModal
                            isOpen={isModalOpen}
                            onClose={toggleModal}
                            onConfirmDelete={handleDeleteConfirmation}
                        >
                            <p>
                                Are you sure you want to release this Pokemon?
                            </p>
                        </DeleteModal>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default ProfilePage
