import { useState } from 'react'
import { useNavigate } from 'react-router-dom'
import { getRandomEncounter } from '../services/api/getServices'
import poke_gate2 from '@/images/poke_gate2.png'
import bugBG from '/bugBG.png'
import dragonBG from '/dragonBG.png'
import electricBG from '/electricBG.png'
import fairyBG from '/fairyBG.png'
import fightingBG from '/fightingBG.png'
import fireBG from '/fireBG.png'
import flyingBG from '/flyingBG.png'
import ghostBG from '/ghostBG.png'
import grassBG from '/grassBG.png'
import groundBG from '/groundBG.png'
import iceBG from '/iceBG.png'
import normalBG from '/normalBG.png'
import poisonBG from '/poisonBG.png'
import psychicBG from '/psychicBG.png'
import rockBG from '/rockBG.png'
import waterBG from '/waterBG.png'
import { Button } from '@/components/ui/button'
import {
    Card,
    CardContent,
    CardDescription,
    CardHeader,
    CardTitle,
} from '@/components/ui/card'

function EncounterPage() {
    const [pokemon, setPokemon] = useState(null)
    const navigate = useNavigate()

    const capitalizeFirstLetter = (string) => {
        return string.charAt(0).toUpperCase() + string.slice(1)
    }

    const getBackgroundImage = (element) => {
        switch (element) {
            case 'bug':
                return bugBG
            case 'dragon':
                return dragonBG
            case 'electric':
                return electricBG
            case 'fairy':
                return fairyBG
            case 'fighting':
                return fightingBG
            case 'fire':
                return fireBG
            case 'flying':
                return flyingBG
            case 'ghost':
                return ghostBG
            case 'grass':
                return grassBG
            case 'ground':
                return groundBG
            case 'ice':
                return iceBG
            case 'normal':
                return normalBG
            case 'poison':
                return poisonBG
            case 'psychic':
                return psychicBG
            case 'rock':
                return rockBG
            case 'water':
                return waterBG
            default:
                return ''
        }
    }

    const handleEncounter = () => {
        if (pokemon) {
            localStorage.setItem('encounteredPokemon', JSON.stringify(pokemon))
            navigate('/capture')
        }
    }

    const handleKeepLooking = async () => {
        const encounteredPokemon = await getRandomEncounter()
        if (encounteredPokemon) {
            setPokemon(encounteredPokemon)
        }
    }

    return (
        <div className="flex flex-col gap-4 ml-[125px] p-4">
            <div className="flex-grow">
                <div className="flex flex-col items-center justify-center w-full">
                    <div className="pb-3">
                        <div
                            className="relative shadow-lg shadow-secondary  h-[400px]"
                            style={
                                pokemon
                                    ? {
                                          backgroundImage: `url(${getBackgroundImage(
                                              pokemon.element
                                          )})`,
                                          backgroundSize: 'cover',
                                          backgroundPosition: 'center',
                                      }
                                    : {}
                            }
                        >
                            <img
                                className={pokemon ? 'pokemon' : 'pokegate'}
                                src={
                                    pokemon
                                        ? pokemon.sprite_url_front
                                        : poke_gate2
                                }
                                alt="Pokemon"
                            />
                        </div>
                        {pokemon && (
                            <Card className="w-[450px] border-[2px] border-[solid] border-[#8cd4d7] bg-[#373c44] rounded-[10px] [box-shadow:0_4px_6px_rgba(0,_0,_0,_0.1)] my-6">
                                <CardHeader>
                                    <CardTitle className="font-bold ">
                                        {capitalizeFirstLetter(pokemon.name)}
                                    </CardTitle>
                                    <CardDescription>
                                        Level {pokemon.lvl}
                                    </CardDescription>
                                </CardHeader>
                                <CardContent className="px-6">
                                    <div className="overflow-hidden border-[2px] border-[solid] border-[#8cd4d7] bg-[#373c44] rounded-[10px]">
                                        <table className="min-w-full table-fixed">
                                            <thead>
                                                <tr>
                                                    <th className="w-1/4 py-2"></th>
                                                    <th className="w-1/4 py-2"></th>
                                                    <th className="w-1/4 py-2"></th>
                                                    <th className="w-1/4 py-2"></th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <td className="font-bold px-6 py-4 whitespace-normal text-right">
                                                        HP :
                                                    </td>
                                                    <td className="px-6 py-4 whitespace-normal text-left">
                                                        {pokemon.hp}
                                                    </td>
                                                    <td className="font-bold  px-6 py-4 whitespace-normal text-right">
                                                        SPD :
                                                    </td>
                                                    <td className="px-6 py-4 whitespace-normal text-left">
                                                        {pokemon.spd}
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td className="font-bold  px-6 py-4 whitespace-normal text-right">
                                                        ATK :
                                                    </td>
                                                    <td className="px-6 py-4 whitespace-normal text-left">
                                                        {pokemon.atk}
                                                    </td>
                                                    <td className="font-bold  px-6 py-4 whitespace-normal text-right">
                                                        DFNS :
                                                    </td>
                                                    <td className="px-6 py-4 whitespace-normal text-left">
                                                        {pokemon.dfns}
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td
                                                        className="text-right font-bold px-6 py-4 whitespace-normal"
                                                        colSpan={2}
                                                    >
                                                        TYPE :
                                                    </td>
                                                    <td
                                                        className="text-left px-6 py-4 whitespace-normal"
                                                        colSpan={2}
                                                    >
                                                        {capitalizeFirstLetter(
                                                            pokemon.element
                                                        )}
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </CardContent>
                            </Card>
                        )}
                    </div>
                </div>
            </div>
            <div className="flex items-center space-x-20 justify-center">
                <Button className="rounded" onClick={handleKeepLooking}>
                    Keep Looking
                </Button>
                {pokemon && (
                    <Button
                        className="rounded"
                        onClick={handleEncounter}
                        disabled={!pokemon}
                    >
                        Encounter
                    </Button>
                )}
            </div>
        </div>
    )
}

export default EncounterPage
