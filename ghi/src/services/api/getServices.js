import axiosInstance from './axiosConfig'

export async function getProfileInfo() {
    try {
        const response = await axiosInstance.get('/api/trainers/profile/mine')
        return response.data
    } catch (error) {
        console.error('There was an error!', error)
        return null
    }
}

export async function getToken() {
    try {
        const response = await axiosInstance.get('/token')
        return response.data
    } catch (error) {
        console.error('There was an error!', error)
        return null
    }
}

export async function getLoginStatus() {
    try {
        const response = await axiosInstance.get('/token')
        return Boolean(response.data)
    } catch (error) {
        console.error('There was an error!', error)
        return null
    }
}

export async function getRandomEncounter() {
    try {
        const response = await axiosInstance.get('/api/pokemon/encounter')
        return response.data
    } catch (error) {
        console.error('There was an error!', error)
        return null
    }
}

export async function getAccountInfo() {
    try {
        const response = await axiosInstance.get('/api/trainers/account/mine')
        return response.data
    } catch (error) {
        console.error('There was an error!', error)
        return null
    }
}

export async function getBallType(ballType) {
    try {
        const response = await axiosInstance.get(`/api/balls/${ballType}`)
        return response.data
    } catch (error) {
        console.error('There was an error!', error)
        return null
    }
}
