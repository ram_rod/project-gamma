import { getProfileInfo } from './src/services/api/getServices'

// export async function tokenLoader() {
//     try {
//         const tokenInfo = await useToken();
//         return { tokenInfo }
//     } catch (error) {
//         console.error("Error getting token:", error);
//         return { tokenInfo: null };
//     }
// }

// export async function reeeLoader() {
//     try {
//         const reInfo = await useRandomEncounter();
//         return { reInfo }
//     } catch (error) {
//         console.error("Error getting random encounter:", error);
//         return { reInfo: null };
//     }
// }

//export async function accountLoader() {
//     try {
//         const accountInfo = await useAccountInfo();
//         return { accountInfo }
//     } catch (error) {
//         console.error("Error getting account info:", error);
//         return { accountInfo: null };
//     }
// }

// export async function ballLoader() {
//     try {
//         const ballInfo = await useBallType();
//         return { ballInfo }
//     } catch (error) {
//         console.error("Error grabbing your balls:", error);
//         return { ballInfo: null };
//     }
// }

export async function profileLoader() {
    try {
        const profileInfo = await getProfileInfo()
        return { profileInfo }
    } catch (error) {
        console.error('Error getting profile info:', error)
        return { profileInfo: [] }
    }
}
